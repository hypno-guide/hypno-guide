# Introduction

Welcome to Hypno Guide!  This book was created by recreational
hypnosis enthusiasts to address a lack of modern, inclusive,
introductory literature on hypnosis.

## What this book is

- **An introductory guide.** The goal of this book is to be accessible
    to people with no prior experience with hypnosis, and be the
    friendly guide we all wish we had when we were first getting
    curious about hypno.

- **A community project.** This book has no one author; we want to
    incorporate a diverse set of perspectives, experiences, and
    knowledge bases into this guide.  It's currently curated by
    [xenofem](https://fedi.xeno.science/xenofem), but anyone is
    welcome to contribute, and we're committed to making sure that
    everyone gets credit for their contributions.  At the bottom of
    each page, we'll always include a complete list of everyone who
    helped write that page.  If you're interested in helping write
    this book, the best way to get involved is to [join our discord
    server](https://discord.gg/UbQaXB9) and chat with us about what
    you'd like to see added.  You can also open issues and
    merge-requests on [our
    gitlab](https://gitlab.com/hypno-guide/hypno-guide/) if you're
    comfortable using version control software, but that's not a
    requirement to contribute.

- **A book for everyone.** We're not going to assume anything about
    the background of the people reading this, or what aspect of
    recreational hypnosis they're interested in.  This is a book for
    people, of all races and genders and abilities and neurotypes, who
    are interested in being hypnotized, hypnotizing others, or just
    learning more about hypnosis.

## What this book isn't

- **A guide to hypnotherapy.** This is a book about *recreational
    hypnosis*, fun things that you can do with your own and others'
    minds for a limited time in a small well-defined scope.  **DO NOT
    TRY TO USE THE THINGS YOU LEARN HERE TO MAKE BROAD-REACHING,
    LONG-TERM CHANGES TO YOUR OWN OR OTHERS' MINDS.** This is a book
    about turning consenting friends into cats for fifteen minutes,
    not a book about helping people quit smoking forever. Using
    hypnosis for therapeutic purposes has a lot of serious pitfalls,
    and can cause a lot of harm if done badly.  Do not give anyone
    hypnotherapy if you are not a professionally trained
    hypnotherapist, and do not accept hypnotherapy from anyone who
    isn't a professionally trained hypnotherapist.

- **A comprehensive guide to all things hypnotic.** There are as many
    ways to do hypnosis as there are people, and we encourage
    exploring and experimenting beyond the material in this book, as
    long as you're staying risk-aware and consent-driven.  If you
    discover something cool and new that you'd like to share, let us
    know!

Welcome, and have fun!

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

This work is licensed under a [Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

#### (contributors: [xenofem](https://fedi.xeno.science/xenofem))
