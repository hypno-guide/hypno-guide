# What is hypnosis?

Hypnosis, broadly speaking, is about bringing people into an altered
headspace called *trance*, and possibly making use of that headspace
to create unique experiences or effects.  Trance has some
commonalities with meditation, dissociation, and the "flow" states
some people get into while deeply engaged in a task.  In general,
trance is a relaxed state where people are less self-conscious, less
inhibited, and more suggestible.  Some of the things that can be done
with trance include:

- Giving people suggestions that they'll follow, even after leaving
  trance
- Creating vivid imaginary sensations and experiences
- Exploring different facets of people's minds and letting the subconscious
  express itself
- Playing with people's personalities and emotions
- Altering memories or perception of time

In later chapters, we'll discuss how people go into trance and how
people work with trance.  This chapter is about what hypnosis and
trance are, what we understand about them, and some common
misconceptions about them.

#### (contributors: anon, [xenofem](https://fedi.xeno.science/xenofem))
