# Myths and facts about hypnosis

In this section, we'll correct and clarify some common misconceptions about hypnosis and trance.

### Myth: You can't be hypnotized to do anything you don't want to do.

While it's true that hypnosis is by and large a collaborative process
between the hypnotist and the hypnotee, that doesn't mean you can't be
made to do things you're uncomfortable with.

To begin with, what any given part of us feels like we might "want" on
some level, at any given moment, is a fluid and complicated thing
that's highly subject to external influence.  Sometimes, bad ideas can
seem fascinating or appealing to some part of us, and manipulative
people with or without hypnosis can subtly coerce us into doing things
that we'd clearly see as undesirable if we had a bit of time to think
about it.

Trance, being an altered state, adds even more complexity.  Some
people compare trance to being lightly drunk in terms of how it
impacts critical thinking and situational awareness.  In addition, if
someone's excited about hypnosis and about the idea of being
hypnotically controlled, then being made to do things they don't
really want to do can sometimes have a certain dangerous allure in and
of itself.  Furthermore, for experienced hypnotees who have a lot of
practice going in and out of trance and have developed powerful
associations with specific types of hypnotic induction, without strong
safeties in place it may be difficult to override an automatic
response to an induction, even in an undesirable context.

This isn't to say that people have no agency while in hypnosis;
hypnotees have a lot of power to exert control over themselves and not
automatically accept hypnotic suggestions as they're given.  A
trustworthy hypnotist is one who affirms and supports that sense of
agency, and some hypnotists even help hypnotees specifically practice
overriding undesirable suggestions.  However, someone who gets
hypnotized in uncomfortable ways that they didn't consent to should
never feel like it's their fault for not resisting, or like deep down
they must have actually wanted what happened to them.

### Myth: Some people are more susceptible to hypnosis than others.

The idea of varying levels of hypnotic susceptibility comes from
taking a one-size-fits-all approach to hypnosis, where if a specific
hypnotic induction brings some people into trance and not others, the
people who went into trance are deemed "more susceptible".  This is a
deeply flawed way of thinking about hypnosis and about people.
Everyone's brain is different, and everyone has a different
relationship with hypnosis and trance.  Techniques that work well for
bringing one person into trance may be totally ineffective for another
person, and vice versa.  Hypnosis is a process of hypnotists and
hypnotees exploring brainspace together, figuring out what resonates
and what doesn't.  It can take time to get a handle on what works well
for any given person, and that's fine.  Nobody should ever be made to
feel like they're a "bad" hypnotee just because something didn't go
the way a hypnotist expected it to go.

#### (contributors: anon, [xenofem](https://fedi.xeno.science/xenofem))
