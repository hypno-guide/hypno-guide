# Definitions

Here are some terms we'll be using in this chapter and throughout the
rest of the book.  Many of these will be discussed in more detail in
later chapters.

- **Hypnosis:** bringing people into trance and/or making use of trance.
- **Trance:** an altered headspace involving relaxation and suggestibility.
- **Hypnotist:** a person who performs hypnosis.
- **Hypnotee:** a person who experiences hypnosis.
- **Induction:** a process for bringing someone into trance.  There
    are many different ways to enter trance, and many kinds of
    inductions, as we'll discuss in chapter 3.
- **Deepener:** a process for helping someone feel more
    strongly/completely in trance.
- **Hypnotic suggestion (often just "suggestion"):** a suggestion,
  reinforced by use of trance, that someone will feel, do, or
  experience something in particular.
- **Fractionation:** repeatedly bringing someone in and out of trance,
  creating a lingering hazy state that persists for some time after
  trance.
- **Abreaction:** an unintended negative emotional response to hypnosis.

#### (contributors: Rigel Troi, [xenofem](https://fedi.xeno.science/xenofem))
