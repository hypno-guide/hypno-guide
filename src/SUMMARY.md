# Summary

[Introduction](intro.md)

- [What is hypnosis?](what-is-hypno/index.md)
  - [Definitions](what-is-hypno/definitions.md)
  - [TODO: Science](what-is-hypno/science.md)
  - [Myths and facts](what-is-hypno/myths-facts.md)

- [TODO: Safety](safety/index.md)
  - [Negotiation and consent](safety/negotiation-consent.md)
  - [Escape hatches](safety/escape-hatches.md)
  - [Abreactions](safety/abreactions.md)

- [TODO: Inductions](inductions/index.md)

- [TODO: Depth and deepeners](depth/index.md)

- [TODO: Hypnotic suggestions](suggestions/index.md)

- [TODO: Tips for hypnotees](hypnotees/index.md)

- [TODO: Tips for hypnotists](hypnotists/index.md)

- [TODO: Community](community/index.md)
  - [Finding groups](community/finding-groups.md)
  - [Vetting people](community/vetting-people.md)
  - [Online safety](community/online-safety.md)
